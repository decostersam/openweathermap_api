﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWeatherMap_API.WeatherItems
{
    public class OpenWeatherMapMain
    {
        [JsonProperty("temp")]
        public double Temp;

        [JsonProperty("pressure")]
        public double Pressure;

        [JsonProperty("humidity")]
        public double Humidity;

        [JsonProperty("temp_min")]
        public double Temp_min;

        [JsonProperty("temp_max")]
        public double Temp_max;

        [JsonProperty("sea_level")]
        public double Sea_level;

        [JsonProperty("grnd_level")]
        public double Grnd_level;

        [JsonProperty("temp_kf")]
        public double Temp_kf;

    }
}
