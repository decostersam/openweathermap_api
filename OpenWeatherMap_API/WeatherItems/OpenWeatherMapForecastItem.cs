﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWeatherMap_API.WeatherItems
{
    public class OpenWeatherMapForecastItem
    {
        [JsonProperty("dt")]
        public int DateTime;

        [JsonProperty("dt_txt")]
        public string DateTime_txt;

        [JsonProperty("main")]
        public OpenWeatherMapMain Main;

        [JsonProperty("weather")]
        public OpenWeatherMapWeather[] Weather;

        [JsonProperty("clouds")]
        public OpenWeatherMapClouds Clouds;

        [JsonProperty("wind")]
        public OpenWeatherMapWind Wind;

        [JsonProperty("sys")]
        public OpenWeatherMapSys Sys;
    }
}
