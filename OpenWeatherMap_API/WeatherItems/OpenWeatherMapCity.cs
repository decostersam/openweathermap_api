﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWeatherMap_API.WeatherItems
{
    public class OpenWeatherMapCity
    {
        [JsonProperty("id")]
        public int Id;

        [JsonProperty("name")]
        public string Name;

        [JsonProperty("coord")]
        public OpenWeatherMapCoord Coord;

        [JsonProperty("country")]
        public string Country;

        [JsonProperty("population")]
        public int Population;

        [JsonProperty("timezone")]
        public int Timezone;

        [JsonProperty("sunrise")]
        public UInt64 sunrise;

        [JsonProperty("sunset")]
        public UInt64 sunset;
    }
}
