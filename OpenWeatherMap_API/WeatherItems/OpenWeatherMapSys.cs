﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWeatherMap_API.WeatherItems
{
    public class OpenWeatherMapSys
    {
        [JsonProperty("pod")]
        public string Pod;

        [JsonProperty("type")]
        public int Type;

        [JsonProperty("id")]
        public int Id;

        [JsonProperty("country")]
        public string Country;

        [JsonProperty("sunrise")]
        public Int64 Sunrise;

        [JsonProperty("sunset")]
        public Int64 Sunset;
    }
}
