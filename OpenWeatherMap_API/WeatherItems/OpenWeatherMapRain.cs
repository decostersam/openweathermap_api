﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWeatherMap_API.WeatherItems
{
    public class OpenWeatherMapRain
    {
        [JsonProperty("3h")]
        public int rain3h;
    }
}
