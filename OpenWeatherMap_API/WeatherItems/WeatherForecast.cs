﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWeatherMap_API.WeatherItems
{
    public class WeatherForecast
    {
        [JsonProperty("code")]
        public int Code;

        [JsonProperty("message")]
        public int Message;

        [JsonProperty("cnt")]
        public int Cnt;

        [JsonProperty("list")]
        public OpenWeatherMapForecastItem[] Items;
    }
}
