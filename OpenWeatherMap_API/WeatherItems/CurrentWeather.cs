﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWeatherMap_API.WeatherItems
{
    public class CurrentWeather
    {
        [JsonProperty("coord")]
        public OpenWeatherMapCoord Coord;

        [JsonProperty("weather")]
        public OpenWeatherMapWeather[] Weather;

        [JsonProperty("base")]
        public string Base;

        [JsonProperty("main")]
        public OpenWeatherMapMain Main;

        [JsonProperty("visibility")]
        public Int32 Visibility;

        [JsonProperty("wind")]
        public OpenWeatherMapWind Wind;

        [JsonProperty("clouds")]
        public OpenWeatherMapClouds Clouds;

        [JsonProperty("dt")]
        public UInt64 DateTime;

        [JsonProperty("sys")]
        public OpenWeatherMapSys Sys;

        [JsonProperty("timezone")]
        public uint Timezone;

        [JsonProperty("id")]
        public uint Id;

        [JsonProperty("name")]
        public string Name;

        [JsonProperty("code")]
        public int Code;
    }
}
