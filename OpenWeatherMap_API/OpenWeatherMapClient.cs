﻿using Newtonsoft.Json;
using OpenWeatherMap_API.WeatherItems;
using System;
using System.Net;

namespace OpenWeatherMap_API
{
    public class OpenWeatherMapClient
    {
        private string apiKey;

        private const string currentWeatherURL = "https://api.openweathermap.org/data/2.5/weather?q={0}&appid={1}";
        private const string weatherForecastURL = "https://api.openweathermap.org/data/2.5/forecast?q={0}&appid={1}";
        public OpenWeatherMapClient(string ApiKey)
        {
            apiKey = ApiKey;
        }

        public CurrentWeather GetCurrentWeather(string Location)
        {
            CurrentWeather weather;
            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString(String.Format(currentWeatherURL, Location, apiKey));
                weather = JsonConvert.DeserializeObject<CurrentWeather>(json);
            }
            return weather;
        }

        public WeatherForecast GetWeatherForecast(string Location)
        {
            WeatherForecast forecast;
            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString(String.Format(weatherForecastURL, Location, apiKey));
                forecast = JsonConvert.DeserializeObject<WeatherForecast>(json);
            }
            return forecast;
        }
    }
}
